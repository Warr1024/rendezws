require("./lib_ecdhfix");

const hpkp = require("./lib_hpkp");
const keepalive = require("./lib_keep");
const logctx = require("./lib_logctx");
const net = require("net");
const ws = require("ws");

var client = { };
module.exports = client;

function tunnel(netconn, uri, ctx, conf) {
	ctx = ctx || new logctx();
	ctx.log("socket open");

	var eof;
	var wsq = [];

	var wsconn = hpkp(ctx, conf, uri, {
		protocol: "binary"
	});
	wsconn.on("open", () => {
		ctx.log("tunnel open");
		wsq.forEach(x => wsconn.send(x));
		wsq = undefined;
		if(eof)
			wsconn.close();
	});
	wsconn.on("close", () => {
		ctx.log("tunnel closed");
		netconn.end();
	});
	wsconn.on("message", data =>
		netconn.write(data));

	netconn.on("data", data => {
		if(wsq)
			wsq.push(data);
		else if(wsconn.readyState == 1)
			wsconn.send(data);
	});
	netconn.on("close", () => {
		ctx.log("socket closed");
		eof = true;
		if(wsconn.readyState >= 1)
			wsconn.close();
	});
	keepalive(wsconn, conf.timeout || 60000, x => ctx.log(x));

	function onerr(type) {
		return err => {
			ctx.log(type + " error: " + err);
			try { netconn.end(); } catch(err) { }
			try { wsconn.terminate(); } catch(err) { }
		};
	}
	netconn.on("error", onerr("socket"));
	wsconn.on("error", onerr("tunnel"));

	return wsconn;
}
client.tunnel = tunnel;

function bind(conf, abort, ctx) {
	if(!conf.name)
		throw("conf missing name");
	if(!conf.url)
		throw("conf missing url");
	var listen = conf.listen;
	if(!listen)
		throw("conf missing listen spec");
	if(!Array.isArray(listen))
		listen = [listen];

	var ctx = new logctx();
	ctx.log(conf.name);
	abort = abort || {};
	var netconn = net.createServer(conf.netopts || {}, s => {
		var subctx = new logctx();
		subctx.log("new client connection from " + ctx.id);
		return tunnel(s, conf.url, subctx, conf);
	});
	netconn.on("error", err => {
		ctx.log("listener error: " + err);
		netconn.close();
		if(!abort.now) {
			ctx.log("retrying...");
			setTimeout(() => bind(conf, abort, ctx), 2000);
		}
	});
	netconn.on("listening", () => ctx.log("listening on "
		+ JSON.stringify(conf.listen)));
	netconn.listen.apply(netconn, listen);
	return () => {
		ctx.log("unconfiguring");
		abort.now = true;
		netconn.close();
	};
}
client.bind = bind;
