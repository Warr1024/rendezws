require("./lib_ecdhfix");

const client = require("./tun_client");
const hpkp = require("./lib_hpkp");
const keepalive = require("./lib_keep");
const logctx = require("./lib_logctx");
const net = require("net");
const ws = require("ws")

var host = { };
module.exports = host;

function bind(conf, abort, ctx) {
	if(!conf.name)
		throw("config missing name");
	if(!conf.url)
		throw("config missing url");

	var parts = conf.url.split("/");
	while((parts.length > 0) && !parts[parts.length - 1].match(/\S/))
		parts.splice(-1);
	if(parts.length < 3)
		throw("url is too short");
	while(parts.length)
		if(parts.pop() == "bind")
			break;
	var rooturl = parts.join("/");

	if(!conf.auth)
		throw("conf missing auth");
	var connect = conf.connect;
	if(!connect)
		throw("conf missing connect");
	if(!Array.isArray(connect))
		connect = [connect];

	var ctx = new logctx();
	ctx.log(conf.name)
	abort = abort || {};
	var master = hpkp(ctx, conf, conf.url, {
		protocol: "binary",
		headers: {Authorization: "Bearer " + conf.auth}
	});
	master.on("error", err => {
		ctx.log("error: " + err);
		master.terminate();
	});
	master.on("open", () =>
		ctx.log("bound to " + conf.url));
	master.on("close", () => {
		ctx.log("unbound from " + conf.url);
		if(!abort.now) {
			ctx.log("retrying...");
			setTimeout(() => bind(conf, abort), 2000);
		}
	});
	master.on("message", token => {
		token = "" + token;

		var subctx = new logctx();
		subctx.log("accepting " + token.substr(0, 8) + " from " + ctx.id);

		var acc = rooturl + "/accept/" + token;
		var connparams = [];
		for(var i = 0; i < connect.length; i++)
			connparams.push(connect[i]);
		var accconn;
		connparams.push(() => { accconn = client.tunnel(sock, acc, subctx, conf); });
		var sock = net.createConnection.apply(net, connparams);
		sock.on("error", err => {
			subctx.log("socket error: " + err);
			sock.destroy()
			if(accconn)
				accconn.terminate();
			try { master.send(token); }
			catch(err) { }
		});
	});
	keepalive(master, conf.timeout || 60000, x => ctx.log(x));

	return () => {
		ctx.log("unconfiguring");
		abort.now = true;
		master.terminate();
	};
}
host.bind = bind;
