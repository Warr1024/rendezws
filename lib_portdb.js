const opts = require("./lib_config");
const crypto = require("crypto");
const fs = require("fs");
const os = require("os");
const path = require("path");

var portdb = { };
module.exports = portdb;

portdb.dbroot = opts.hubroot;

function dbpath(name) { return path.join(portdb.dbroot, name + ".json"); }
portdb.dbpath = dbpath;

function randstr(len) {
	var str = "";
	while(str.length < len)
		str += crypto.randomBytes(16)
			.toString("base64")
			.replace(/[^A-Za-z0-9]/g, "");
	return str.substr(0, len);
}
portdb.randstr = randstr;

function checkname(str) {
	return str == str.replace(/[^A-Za-z0-9]/, "");
}
portdb.checkname = checkname;

function load(name, cb) {
	if(!checkname(name))
		return cb("invalid port name");
	var p = dbpath(name);
	fs.readFile(p, (err, data) => {
		if(err)
			return cb(err);
		try { data = JSON.parse(data); }
		catch(err) { return cb(err); }
		return cb(undefined, data);
	});
};
portdb.load = load;

function create(name, cb) {
	name = name || randstr(8);
	if(!checkname(name))
		return cb("invalid port name");
	load(name, (err, data) => {
		if(!err && data)
			return cb(undefined, data);
		data = {
			name: name,
			key: randstr(20),
			created: new Date().getTime() / 1000
		};
		fs.mkdir(portdb.dbroot, 0o700, () => {
			fs.writeFile(dbpath(name), JSON.stringify(data), (err) => {
				if(err)
					return cb(err);
				data.new = true;
				return cb(undefined, data);
			});
		});
	});
};
portdb.create = create;
