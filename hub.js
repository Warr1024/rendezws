const keepalive = require("./lib_keep");
const logctx = require("./lib_logctx");
const portdb = require("./lib_portdb");
const opts = require("./lib_config");
const ws = require("ws");

var bound = { };
function getport(name) { return bound[name] = bound[name] || []; }

var connecting = { };

function hub_bind(ctx, portdata, fulladdr) {
	ctx.hub_name = portdata.name;
	getport(fulladdr).push(ctx);
	ctx.conn.on("close", () => {
		bound[fulladdr] = getport(portdata.name).filter(x => x != ctx);
		ctx.log("unbound from " + fulladdr);
	});
	ctx.conn.on("message", msg => {
		if(typeof(msg) != "string")
			return;
		var c = connecting[msg];
		if(c) {
			if(c.conn)
				c.conn.terminate();
			delete(connecting[msg]);
		}
	});
	keepalive(ctx.conn, opts.timeout, x => ctx.log(x));
	ctx.log("bound to " + fulladdr);
}

function hub_open(ctx, host) {
	var token = portdb.randstr(20);
	while(connecting[token])
		token = portdb.randstr(20);
	ctx.hub_token = token;
	connecting[token] = ctx;

	ctx.q = [];
	ctx.conn.on("message", x => {
		if(ctx.q)
			ctx.q.push(x);
	});
	ctx.conn.on("close", () => {
		if(connecting[token] != ctx)
			return;
		delete(connecting[token]);
		ctx.log("aborted");
	});

	if(host.conn.readyState == 1)
		host.conn.send(token);

	ctx.log("connecting to " + host.hub_name);

	ctx.timeout = setTimeout(() => {
		ctx.log("timed out");
		ctx.conn.terminate();
	}, opts.timeout);
}

function hub_accept(host, client) {
	host.log("established from " + client.id);
	delete(connecting[client.hub_token]);

	clearTimeout(client.timeout);
	if(client.q) {
		if(host.conn.readyState == 1)
			client.q.forEach(x => host.conn.send(x));
		delete(client.q);
	}

	[
		{name: "c2h", to: host, from: client},
		{name: "h2c", to: client, from: host}
	].forEach(dir => {
		keepalive(dir.from.conn, opts.timeout, x => dir.from.log(x));

		dir.from.conn.on("message", data => {
			if(dir.to.conn.readyState == 1)
				dir.to.conn.send(data);
		});
		dir.from.conn.on("close", () => {
			dir.from.log("closed");
			dir.to.conn.close();
		});
		dir.from.conn.on("error", err => {
			dir.to.conn.terminate();
			dir.from.conn.terminate();
		});
	});
}

opts.handleProtocols = (p) => {
	if(p.some((x) => x == "binary"))
		return "binary";
	return false;
};
opts.verifyClient = (info, cb) => {
	var ctx = new logctx();
	ctx.log("new connection");

	var logcb = (ok, code, msg) => {
		ctx.log((code ? (code + " ") : "") + msg);
		return ok ? cb(ok) : cb(ok, code, msg);
	};

	var req = info.req;
	req.hub_ctx = ctx;
	var parts = req.url.split("/").filter(x => x.match(/\S/));
	if(opts.strip)
		parts.splice(0, opts.strip);
	if(parts.length < 2)
		return cb(false, 400, "invalid uri");

	if(parts[0] == "open") {
		var host = bound[parts.slice(1).join("/")]
		if(!host || (host.length < 1))
			return logcb(false, 404, "port not bound");
		host = host[(host.length < 2) ? 0 : Math.floor(Math.random() * host.length)]
		ctx.hub_cb = c => hub_open(c, host);
		return logcb(true, 0, "open " + parts.slice(1).join("/"));
	}
	if(parts[0] == "bind") {
		var auth = req.headers.authorization;
		if(!auth)
			return logcb(false, 401, "no auth");
		auth = auth.split(" ");
		if((auth.length != 2) || (auth[0] != "Bearer"))
			return logcb(false, 401, "bad auth type");

		return portdb.load(parts[1], (err, data) => {
			if(err || (data.key != auth[1]))
				return logcb(false, 401, "auth mismatch");
			var fulladdr = parts.slice(1).join("/");
			ctx.hub_cb = c => hub_bind(c, data, fulladdr);
			return logcb(true, 0, "bind " + fulladdr);
		});
	}
	if(parts[0] == "accept") {
		var client = connecting[parts[1]];
		if(!client)
			return logcb(false, 404, "client not found");
		ctx.hub_cb = c => hub_accept(c, client);
		return logcb(true, 0, "accept " + parts[1].substr(0, 8));
	}

	return logcb(false, 400, "invalid uri");
};

var hub = new ws.Server(opts);
hub.on("listening", () => console.log("[master] listening on " + opts.port));
hub.on("error", err => { throw(err); });

hub.on("connection", (conn, req) => {
	var ctx = req.hub_ctx;
	if(!ctx)
		return conn.terminate();
	conn.on("error", err => {
		ctx.log("error: " + err);
		conn.terminate();
	});
	ctx.conn = conn;
	ctx.req = req;
	if(ctx.hub_cb)
		return ctx.hub_cb(ctx);
	return conn.terminate();
});
