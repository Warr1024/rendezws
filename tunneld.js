const opts = require("./lib_config");
const client = require("./tun_client");
const host = require("./tun_host");
const fs = require("fs");
const glob = require("glob");
const path = require("path");
const underscore = require("underscore");

configs = [];

function procconf(conf) {
	for(var i = 0; i < configs.length; i++)
		if(underscore.isEqual(conf, configs[i].conf)) {
			var c = configs.splice(i, 1)[0];
			c.kept = true;
			return c;
		}
	conf.conf = Object.assign({}, conf);
	if(conf.type == "host") {
		conf.name = "host " + JSON.stringify(conf.url) + " -> " + JSON.stringify(conf.connect);
		conf.bind = () => host.bind(conf);
		return conf;
	}
	if(conf.type == "client") {
		conf.name = "client " + JSON.stringify(conf.listen) + " -> " + JSON.stringify(conf.url);
		conf.bind = () => client.bind(conf);
		return conf;
	}
	console.log("unsupported conf type for " + JSON.stringify(conf));
}

function loadconf(data) {
	var newconf = [];
	for(var i = 0; i < data.length; i++) {
		var c = procconf(data[i]);
		if(!c)
			continue;
		console.log("[config] add " + c.name + (c.kept ? " (retained)" : ""));
		newconf.push(c);
	}
	for(var i = 0; i < configs.length; i++) {
		var c = configs[i];
		console.log("[config] del " + c.name);
		c.rm();
	}
	configs = newconf;
	for(var i = 0; i < configs.length; i++)
		if(!configs[i].rm) {
			configs[i].rm = configs[i].bind();
			delete(configs[i].bind);
		}
}

function loadconcat(confs) {
	console.log("reload configs: " + JSON.stringify(confs));
	var pend = {};
	var all = [];
	for(var i = 0; i < confs.length; i++)
		(function(f) {
			pend[f] = true;
			fs.readFile(f, (err, data) => {
				if(err)
					console.log(f + ": " + err);
				else
					try {
						var o = JSON.parse(data);
						if(Array.isArray(o))
							for(var j = 0; j < o.length; j++)
								all.push(o[j]);
						else
							all.push(o);
					} catch(err) {
						console.log(f + ": " + err);
					}
				delete pend[f];
				for(var k in pend)
					if(pend.hasOwnProperty(k))
						return;
				return loadconf(all);
			});
		})(confs[i]);
}

var stats = {};
var lastconf = "";
function rescan() {
	var confs = [];
	var dirty = false;
	return new glob.Glob("**/*.json", {
		cwd: opts.tunroot,
		stat: true,
		dot: false,
		nosort: true,
		nocase: true,
		matchBase: true,
		nodir: true,
		follow: true
	}).on("stat", (f, s) => {
		f = path.join(opts.tunroot, f);
		confs.push(f);
		if(underscore.isEqual(stats[f], s))
			return;
		dirty = true;
		stats[f] = s;
	}).on("end", () => {
		confs.sort();
		var nowconf = JSON.stringify(confs);
		if(nowconf != lastconf)
			dirty = true;
		lastconf = nowconf;
		if(!dirty) return;
		return loadconcat(confs);
	});
}

rescan();
setInterval(rescan, 5000);
