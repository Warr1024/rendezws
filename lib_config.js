const minimist = require("minimist");
const os = require("os");
const path = require("path");

const defs = {
	port: 8080,
	timeout: 60000,
	hubroot: path.join(os.homedir(), ".rendezws-hub"),
	tunroot: path.join(os.homedir(), ".rendezws-tun")
};

var opts = minimist(process.argv.slice(2), {default: defs});
module.exports = opts;

console.log("opts: " + JSON.stringify(opts));
