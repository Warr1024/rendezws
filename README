========================================================================
        rendezws: Universal TCP-over-WebSocket Tunneling Toolkit
------------------------------------------------------------------------

rendezws allows forward and reverse tunneling of TCP connections via an
intermediary "hub" web service.  The only protocol in use is HTTP[s].
This combination creates ideal conditions for firewall traversal.

------------------------------------------------------------------------
INSTALLATION

Clone the project and install npm dependencies:

	git clone https://gitlab.com/Warr1024/rendezws.git rendezws &&\
	cd rendezws &&\
	npm install

------------------------------------------------------------------------
OPERATING A HUB

To create tunnels, first you need to have access to a "hub" web service
that can be reached by all tunnel endpoints.

........................................................................
SETUP THE HUB

To run the hub web service, you can simply run:

	node hub

It also accepts options as via the command line to override internal
defaults; refer to the node ws documentation for most of these.

	node hub --port=8280 --timeout=15000

NOTE that it is recommended to run the hub behind a reverse proxy, such
as nginx, to provide TLS, as well as the ability to integrate the hub
with an existing site.  TLS support in the hub app is not currently
planned (though pull requests may be welcome).

........................................................................
CREATE TUNNEL HOST ENDPOINTS

Services can only be hosted on a hub by invitation.  To create a host
account, run:

	node mkport [name]

[name] is an optional name for the endpoint (called a "port"
internally).  It must be alphanumeric, and is case-sensitive.  If none
is provided, a random port name will be created, which can be useful
for "hidden" services.

The new endpoint's details will output, including the "key" value, which
is needed by the tunnel host to bind to that address.

Recreating an endpoint that already exists will just display the
existing information; you can tell if the endpoint was newly created or
existing based on the "new" key that only appears in output for new
endpoints.

------------------------------------------------------------------------
CREATING TUNNELS

Tunnels are for both hosting services and connecting to them.  All
tunnels endpoints, both host and client, only need to be able to reach
the web address at which the hub is running.

Running the tunnel server:

	node tunneld

A single tunnel server can serve multiple both host and client tunnel
specs.  By default, the tunnel server will scan for valid JSON files in
~/.rendezws-tub, and update automatically on config changes.  Each file
should contain either a tunnel spec, or an array of tunnel specs.
Invalid files are ignored.  Tunnel specs are described below.  Avoid
exact duplicate specs.

........................................................................
HOSTING A SERVICE

A host tunnel spec takes the following form:

	{ "type":    "host",
	  "url":     "ws://hostname/path-if-necessary/bind/port",
	  "auth":    "auth-token-for-port",
	  "connect": [80, "127.0.0.1"]
	}

url can be ws:// or wss:// (for TLS).  See the common options below for
extra TLS security options.  The base url at which the rendezws hub is
hosted should have /bind/ suffixed, followed by the port name.

connect can be either an array containing [TCP Port, Address], or a
string for a unix-domain socket path.

auth is the auth key assigned by the hub to access this endpoint.

A websocket tunnel will be created to the hub to setup the binding for
the service, and new connection tokens will be send over it.  For each
incoming connection, the host tunnel will create another outbound
websocket connection to the hub to carry each individual tunneled
connection.

Note that multiple tunnel applications can bind to the same service on
the hub, and the hub will randomly assign hosts to handle connections.
This can be used for redundancy and load balancing.

Each port can also host separate services using shared auth, by adding
additional path components after the port name, e.g. port/subport/etc.

........................................................................
CONNECTING TO A SERVICE

A client tunnel spec takes the following form:

	{ "type":   "client",
	  "listen": [22, "0.0.0.0"],
	  "url":    "ws://hostname/path-if-necessary/open/port"
	}

listen is the endpoint to listen on locally, in the form of either an
array of [TCP Port, Address], or a string path for unix-domain sockets.

url can be ws:// or wss:// (for TLS).  See the common options below for
extra TLS security options.  The base url at which the rendezws hub is
hosted should have /bind/ suffixed, followed by the port name.

The tunnel application will wait for incoming TCP connections, and
then create one outbound websocket tunnel for each one.

........................................................................
COMMON OPTIONS

These extra optional parameters are available for both host and client
tunnel specs:

	{ "pins": { "pinsha256": true, "anothersha256": true },
	  "nopki": true
	}

The pins option allows specifying public key pins, in the same format
as HTTP Public Key Pinning (pin-sha256), for pinning to specific public
keys.  This is very useful to establish a secure connection with a hub
server using self-signed certificates (which requires PKI validation to
be disabled) but can add extra security to almost any setup.  The pins
object keys should each be a pin-sha256 for the server, and the values
should all be true.

The nopki option, if set to true, disables normal certificate checking,
allowing use of wss:// connections to servers with untrusted TLS
certificates.  The key pinning mechanism above is recommended to replace
PKI security when this option is enabled.

------------------------------------------------------------------------
TUNCAT TOOL

The tuncat tool provides a quick way to connect stdin/stdout directly
to a websocket tunnel:

	node tuncat --url=ws://host/path/open/port

This can be used like a replacement for netcat, including in openssh
ProxyCommands, or serving tunnels via inetd.

Command line parameters are parsed by minimist, and some of the same
options described above (such as HTTPS pins and nopki) are supported.

------------------------------------------------------------------------
SECURITY

Use of HTTPS (wss://) on the hub web server is recommended to protect
host service binding credentials, and to avoid exposing the existence
of endpoints.

Use of some secure, authenticated protocol such as TLS or SSH over
connections through the tunnels is recommended to avoid eavesdropping
and spoofing by an untrustworthy and/or potentially compromised hub
provider.

------------------------------------------------------------------------
COMPATIBILITY

Client tunnels (and tuncat) are believed to be compatible with:
  - websockify (https://github.com/kanaka/websockify)
  - websocketd (http://websocketd.com/)

========================================================================
