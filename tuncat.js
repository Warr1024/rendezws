const hpkp = require("./lib_hpkp");
const opts = require("./lib_config");
const net = require("net");
const ws = require("ws");

if(!opts.url)
	throw("--url must be specified");
opts.protocol = "binary";

var wsq = [];
var eof;

var wsconn = hpkp({
	log: x => console.log(x)
}, opts, opts.url, opts);
wsconn.on("error", err => {
	console.log(err);
	process.exit(1);
});
wsconn.on("open", () => {
	wsq.forEach(x => wsconn.send(x));
	wsq = undefined;
	if(eof)
		wsconn.close();
});
wsconn.on("message", data => process.stdout.write(data));
process.stdin.on("data", data => {
	if(wsq)
		wsq.push(data);
	else
		wsconn.send(data);
});

process.stdin.on("close", () => {
	eof = true;
	if(wsconn.readyState >= 1)
		wsconn.close();
});
wsconn.on("close", () => process.exit(0));
